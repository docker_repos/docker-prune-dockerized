FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

ENV USERNAME=root
ENV CRON_TIME="*/5 * * * *"
ENV TZ=Europe/Amsterdam
ENV PRUNE_TYPE=system
ENV PRUNE_PARAM=

COPY ./root/ /

RUN apt update
RUN apt install cron tzdata docker.io -y

ENTRYPOINT ["sh", "/entrypoint.sh"]
