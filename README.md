# Docker-prune Dockerized
GitLab: [https://gitlab.com/docker_repos/docker-prune-dockerized](https://gitlab.com/docker_repos/docker-prune-dockerized)  
DockerHub: [https://hub.docker.com/r/mega349/docker-prune-dockerized](https://hub.docker.com/r/mega349/docker-prune-dockerized)

---

## Description
Periodically prune unneeded leftovers from docker, using cron and docker prune command.  
The used command will be build from env vars:  
`docker $PRUNE_TYPE prune -f $PRUNE_PARAM`

---

## ATTENTION!

Using **docker prune will delete** thinks, **based on your setup**!  
Using the **default settings, will delete unused** (including temporary unused): **containers, networks, images, build-caches**  

**Read the official doku and take care!**

---

## Example Docker Compose file
```yml
version: '3.8'

services:
  app:
    image: mega349/docker-prune-dockerized:latest
    restart: on-failure
    volumes:
          - /var/run/docker.sock:/var/run/docker.sock
          - /var/lib/docker:/var/lib/docker:ro
    environment:
      - TZ=Europe/Amsterdam
      - CRON_TIME=*/5 * * * *
      - USERNAME=root
      - PRUNE_TYPE=system
      - PRUNE_PARAM=--volumes  # NOT default, but an example
```

---

## Environment vars
| ENV           | Description                                 | Info                                     | default value      | NEEDED |
| ------------- | ------------------------------------------- | ---------------------------------------- | ------------------ | ------ |
| `TZ`          | Your Timezone                               | Timezone identifier                      | `Europe/Amsterdam` | No     |
| `CRON_TIME`   | Backup time in cron syntax                  |                                          | `*/5 * * * *`      | No     |
| `USERNAME`    | Run command as given user                   |                                          | `root`             | No     |
| `PRUNE_TYPE`  | What type to prune                          | Take a look at [Prune typs](#prune-typs) | `system`           | No     |
| `PRUNE_PARAM` | Additional parameter for used prune command | Take a look at the official docker doku  |                    | No     |

---

## Prune typs
Please check the official doku for effects and additional parameter

| Type        | Description               | Official Doku                                                                            |
| ----------- | ------------------------- | ---------------------------------------------------------------------------------------- |
| `system`    | everythink except volumes | [system_prune](https://docs.docker.com/engine/reference/commandline/system_prune/)       |
| `network`   | only networks             | [network_prune](https://docs.docker.com/engine/reference/commandline/network_prune/)     |
| `image`     | only images               | [image_prune](https://docs.docker.com/engine/reference/commandline/image_prune/)         |
| `container` | only container            | [container_prune](https://docs.docker.com/engine/reference/commandline/container_prune/) |
| `volume`    | only volumes              | [volume_prune](https://docs.docker.com/engine/reference/commandline/volume_prune/)       |

---
