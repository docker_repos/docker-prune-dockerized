#!/bin/bash

export DOCKER_DIR=/var/lib/docker

echo "---- App started ----"
starttime=$SECONDS # Count runtime

echo "##############################################################"

echo "Docker foldersize:"
du -hd0 $DOCKER_DIR 2>/dev/null
du -hd0 $DOCKER_DIR/* 2>/dev/null | sort -hr

echo "##############################################################"

echo "Total:"
df -h $DOCKER_DIR

echo "##############################################################"

export prunecmd=$(echo -n "$PRUNE_TYPE prune -f $PRUNE_PARAM")
echo "using command: \"docker $prunecmd\""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
docker $prunecmd

echo "##############################################################"

echo "New Total:"
df -h $DOCKER_DIR

echo "##############################################################"

echo "--- App stopped after $(( SECONDS - starttime )) seconds! ---"
