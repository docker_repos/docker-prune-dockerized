#!/bin/bash

# Creating crontab and export envs to crontab
env > /etc/environment
echo "${CRON_TIME} ${USERNAME} /cron-entrypoint.sh >/proc/1/fd/1 2>/proc/1/fd/1" | sed -r "s~\"~~" | sed -r "s~\"~~" >> /etc/crontab

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Waiting for cron [${CRON_TIME}]"
cron -f
